# Заголовок

**Эта программа позволяет:**

- Отслеживать кол-во цветов на складе.

- Создавать-заказывать новые экземляры.

- Удалять экземпляры.

- Покупать цветки.

- Покупать букеты.

**Список классов:**
- [ ] Flower
- [ ] Boquet

**Файл с данными храниться в формате pickle**

    try:
        with open('data.pickle', 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        print('Файл не найден')
        exit()
_Цена букета находится по формуле:_

$cost_boq += i.cost * x$

[Видео](https://www.youtube.com/watch?v=7aWT4xl61t8&t=481s)

![котик](https://sun9-56.userapi.com/impg/dsKBNu8LSISHdg8OiVrHO08yBMV8ZoOWjD4XLQ/xgiAZ5yfVzY.jpg?size=1169x1169&quality=95&sign=24eab05645b241d96401f69f4505da91&type=album)


