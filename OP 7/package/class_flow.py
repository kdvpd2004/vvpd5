class Flower:
    """Класс Flower, описывающий цветки.
    Atributs
    --------
    name - наименование экземпляра цветка(str)
    color - цвет экземпляра цветка(str)
    lenght - длина экземпляра цветка(int)
    cost - цена за штуку экземпляра цветка(int)
    count - кол-во оставшихся цветков экземпляра(int)

    Methods
    --------
    subtraction(self, times):
    отнимает от атрибута count единицу
    :arg times - кол-во раз отниманий единиц
    """

    def __init__(self, name, color, lenght, cost, count):
        self.name = name
        self.color = color
        self.lenght = lenght
        self.cost = cost
        self.count = count

    def __str__(self):
        return f'Цветок:{self.name}\n' \
               f'цвет:{self.color}\n' \
               f'длина:{self.lenght}\n' \
               f'цена:{self.cost}\n' \
               f'кол-во на складе:{self.count}'

    def subtraction(self, times):
        self.count -= 1 * times
        return self.count

    class Bouquet:
        """Подклас описывающий букет состоящий из нескольких цветов.
        Atributs
        --------
        count_flow - кол-во различных экземпляров класса Flower,
         из которых состоит букет(int)
        args - экземпляры класса Flower(tuple)

        Methods
        --------
        boq(self)- запрашивает на ввод кол-во штук определённого
        экземпляра класса Flower в букете и возвращает стоимость букета и кол-во цветков.
        """

        def __init__(self, count_flow, *args):
            self.count_flow = count_flow
            self.args = args

        def __str__(self):
            a = []
            for i in self.args:
                a.append(i.name)
            return f'букет, состоящий из цветков:{a}\n' \
                   f'Кол-во цветов и их стоимость:{self.boq}'

        @property
        def boq(self):
            n = []
            cost_boq = 0
            for i in self.args:
                x = int(input(f'Введите кол-во штук цветка {i.name}: '))
                n.append(x)
                cost_boq += i.cost * x
                Flower.subtraction(i, x)
            n = sum(n)
            return n, cost_boq
