import pickle
from package.class_flow import Flower


def load_pickle():
    """Функция читает данные из файла data.pickle
    :return: None"""
    try:
        with open('data.pickle', 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        print('Файл не найден')
        exit()


def print_flowers(some):
    """Функция вывода атрибутов экземпляров класса для чтения.
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    print('Ассортимент цветов:', end='\n\n')
    for i in some:
        print(i, end='\n\n')


def record_info(some):
    """Функция записи информации на файл data.pickle
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    with open('data.pickle', 'wb') as f:
        pickle.dump(some, f)
    load_pickle()


def request_flower(some):
    """Функция вывода на экран экземпляров класса Flower
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    print('[stop]Остановка процедуры')
    while True:
        name = input('Введите название цветка: ')
        if name == 'stop':
            break
        if not name.isalpha():
            print('Ошибка, введите наименование цветка', end='\n\n')
        color = input('Введите цвет цветка: ')
        if color == 'stop':
            break
        if not color.isalpha():
            print('Ошибка, введите цвет цветка', end='\n\n')
        lenght = input('Введите длину цветка: ')
        if lenght == 'stop':
            break
        if not lenght.isdigit():
            print('Ошибка, введите длину цветка', end='\n\n')
        cost = input('Введите цену цветка: ')
        if cost == 'stop':
            break
        if not cost.isdigit():
            print('Ошибка, введите цену цветка', end='\n\n')
        count = input('Введите кол-во экземпляров цветка: ')
        if count == 'stop':
            break
        if not count.isdigit():
            print('Ошибка, введите кол-во экземпляров цветка', end='\n\n')
        new_flower = Flower(name, color, lenght, cost, count)
        some.append(new_flower)
        print(f'Цветок {new_flower.name} доставлен на склад.', end='\n\n')
        record_info(some)


def buy_flower(some):
    """Функция продажи 1 цветка любого экземпляра
    с помощью метода subtraction(self, times) класса Flowers
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    print('[stop]Остановка процедуры')
    while True:
        flower_name = input('Введите название цветка: ')
        if flower_name == 'stop':
            break
        if flower_name not in list_flower_name(some):
            print('Данного товара нет в ассортименте.', end='\n\n')
            continue
        for i in some:
            if i.name == flower_name:
                print(f'Вы уверены что хотите купить цветок {i.name} за {i.cost}?\n'
                      f'[1]Да|[2]Нет')
                while True:
                    ans = input('> ')
                    if ans == '1':
                        Flower.subtraction(i, 1)
                        record_info(some)
                        print('Спасибо за покупку!')
                        break
                    elif ans == '2':
                        break
                    else:
                        print('Ошибка, введите корректную команду', end='\n\n')
                        continue


def list_flower_name(some):
    """Функция, возвращающая список имён всех
    экземпляров класса
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: p - Список имён цветков"""
    p = []
    for i in some:
        p.append(i.name)
    return p


def del_flower(some):
    """Функция удаления экземляра класса из файла pickle
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    while True:
        print('[stop]Прекращение процедуры')
        name_flow = input('Введите удаляемый экземпляр: ')
        if name_flow == 'stop':
            break
        if not name_flow.isalpha():
            print('Ошибка, введите наименование цветка', end='\n\n')
            continue
        if name_flow not in list_flower_name(some):
            print('Данного товара нет в ассортименте.', end='\n\n')
        for i in range(len(some)):
            if some[i].name == name_flow:
                print('Вы уверены, что хотите удалить данный экземляр?\n'
                      '[1]Да|[2]Нет')
                while True:
                    ch = input('> ')
                    if ch == '1':
                        print('Экземпляр успешно удалён.', end='\n\n')
                        some.pop(i)
                        record_info(some)
                        break
                    elif ch == '2':
                        break
                    else:
                        print('Ошибка, введите корректную команду', end='\n\n')
                        continue


def buy_boq(some):
    """Функция покупки букета, реализованная с помощью
    подкласса класса Flower - Bouquet, и с помощь. его метода boq(self).
    :arg some - массив, содержащий различные
    экземляры класса Flower
    :return: None"""
    print('[stop]Остановка процедуры')
    while True:
        boq = []
        count = input('Введите кол-во экземпляров цветков в букете: ')
        if count == 'stop':
            break
        if not count.isdigit():
            print('Ошибка, введите натуральное число', end='\n\n')
            continue
        if int(count) > len(some):
            print('Число желаемых экземпляров превышает число оставшихся.', end='\n\n')
            continue
        if int(count) < 1:
            print('Ошибка, букет должен состоять из цветка одного экземпляра и более', end='\n\n')
            continue
        flag = False
        for i in range(int(count)):
            if flag:
                break
            print('[back]Вернуться назад')
            while True:
                print(f'Введите название экземляра {i + 1}:')
                flow = input('>')
                print()
                if flow == 'back':
                    flag = True
                    break
                if not flow.isalpha():
                    print('Ошибка, введите корректное название цветка', end='\n\n')
                    continue
                if flow not in list_flower_name(some):
                    print('Данного товара нет в ассортименте.', end='\n\n')
                    continue
                for f in some:
                    if f.name == flow:
                        boq.append(f)
                break
        while True:
            print(f'Уверены ли вы, что желаете приобрести {Flower.Bouquet(count, *boq)}\n'
                  f'[1]Да|[2]Нет')
            choise = input('> ')
            if choise == '1':
                print('Спасибо за покупку!', end='\n\n')
                record_info(some)
                break
            elif choise == '2':
                break
            else:
                print('Ошибка,введите корректную команду', end='\n\n')
                continue


def main():
    """Меню"""
    flowers = load_pickle()
    menu = {'1': ('[1]вывести информацию о имеющихся цветках на экран', print_flowers),
            '2': ('[2]заказать цветок', request_flower),
            '3': ('[3]купить цветок', buy_flower),
            '4': ('[4]купить букет', buy_boq),
            '5': ('[5]удалить данные о экземляре', del_flower)}
    while True:
        print('Выберите команду:')
        print(*[alias for alias, func in menu.values()], sep='\n')
        print()
        print('[exit]выход из программы')
        com = input('> ')
        print()
        if com == 'exit':
            break
        if com not in menu:
            print('Введите корректную команду')
            continue
        func = menu[com][1]
        func(flowers)


if __name__ == '__main__':  # Точка входа
    main()
